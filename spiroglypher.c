#define PIC_WIDTH             5001
#define PIC_HEIGHT            5001
#define SPIRAL_DENSITY        0.35
#define ANGLE_DIFF              45
#define SPIRAL_GRAYSCALE       127
#define MODULATION_FORCE        25
#define MODULATION_POS_OFFSET    5
#define MODULATION_NEG_OFFSET   15

/* don't change these constans */
#define _GNU_SOURCE
#define SUCCESS       0
#define ERROR         1
#define LUMINANCE     0
#define FULL_CIRCLE 360

#define MIN(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

#define MAX(a,b) \
  ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "lodepng/lodepng.h"
#include "ujpeg/ujpeg.h"

typedef struct{
  uint16_t x;
  uint16_t y;
} position_t;

void init_position(
  position_t *this
)
{
  this->x = UINT16_MAX;
  this->y = UINT16_MAX;
}

bool is_position_empty(
  const position_t this
)
{
  return UINT16_MAX == this.x && UINT16_MAX == this.y;
}

void draw_line_x(
  const position_t  a,
  const position_t  b,
  const uint16_t    picture_width,
        uint8_t    *picture_map
)
{
  if(a.x < b.x)
  {
    const double l_slope = (double)(b.y - a.y) / (double)(b.x - a.x);

    for(uint16_t x = a.x; x <= b.x; x++)
    {
      picture_map[(uint32_t)(a.y + (x - a.x) * l_slope) * picture_width + x] = SPIRAL_GRAYSCALE;
    }
  }
  else
  {
    const double l_slope = (double)(a.y - b.y) / (double)(a.x - b.x);

    for(uint16_t x = b.x; x <= a.x; x++)
    {
      picture_map[(uint32_t)(b.y + (x - b.x) * l_slope) * picture_width + x] = SPIRAL_GRAYSCALE;
    }
  }
}

void draw_line_y(
  const position_t  a,
  const position_t  b,
  const uint16_t    picture_width,
        uint8_t    *picture_map
)
{
  if(a.y < b.y)
  {
    const double l_slope = (double)(b.y - a.y) / (double)(b.x - a.x);

    for(uint16_t y = a.y; y <= b.y; y++)
    {
      picture_map[y * picture_width + a.x + (uint32_t)((y - a.y) / l_slope)] = SPIRAL_GRAYSCALE;
    }
  }
  else
  {
    const double l_slope = (double)(a.y - b.y) / (double)(a.x - b.x);

    for(uint16_t y = b.y; y <= a.y; y++)
    {
      picture_map[y * picture_width + b.x + (uint32_t)((y - b.y) / l_slope)] = SPIRAL_GRAYSCALE;
    }
  }
}

void draw_line(
  const position_t  a,
  const position_t  b,
  const uint16_t    picture_width,
        uint8_t    *picture_map
)
{
  if(a.x == b.x)
  {
    for(uint16_t y = MIN(a.y, b.y); y <= MAX(a.y, b.y); y++)
    {
      picture_map[y * picture_width + a.x] = SPIRAL_GRAYSCALE;
    }
  }
  else if(a.y == b.y)
  {
    for(uint16_t x = MIN(a.x, b.x); x <= MAX(a.x, b.x); x++)
    {
      picture_map[a.y * picture_width + x] = SPIRAL_GRAYSCALE;
    }
  }
  else if(abs(a.x - b.x) > abs(a.y - b.y))
  {
    draw_line_x(a, b, picture_width, picture_map);
  }
  else
  {
    draw_line_y(a, b, picture_width, picture_map);
  }
}

void create_spiral_pair(
  const uint16_t    picture_width,
  const uint16_t    picture_height,
  const uint8_t    *modulation_map,
        double      angle,
        uint8_t    *picture_map,
        position_t *first_spiral_start,
        position_t *second_spiral_start
)
{
  uint16_t l_rotation = 0;
  double l_angle = 0;
  double l_radius = 0;
  double l_modulation = 0;
  double l_angle_step = 0;
  bool l_is_last_rotation = false;
  position_t l_pixel;
  position_t l_old_pixel;
  position_t l_last_pixel_in_first_rotation;

  init_position(&l_pixel);
  init_position(&l_old_pixel);
  init_position(&l_last_pixel_in_first_rotation);

  for(l_rotation = 0; FULL_CIRCLE * SPIRAL_DENSITY * l_rotation + FULL_CIRCLE * SPIRAL_DENSITY < MIN(picture_width / 2 - 10, picture_height / 2 - 10); l_rotation++)
  {
    l_angle_step = FULL_CIRCLE * atan(1 / (FULL_CIRCLE * SPIRAL_DENSITY * (l_rotation + 1))) / (2 * M_PI);

    for(l_angle = 0; l_angle < FULL_CIRCLE; l_angle += l_angle_step)
    {
      l_radius = FULL_CIRCLE * SPIRAL_DENSITY * l_rotation + l_angle * SPIRAL_DENSITY;

      l_pixel.x = (uint16_t)(l_radius * sin(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_width / 2 + 1);
      l_pixel.y = (uint16_t)(l_radius * cos(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_height / 2 + 1);

      l_modulation = MODULATION_FORCE * modulation_map[l_pixel.y * picture_width + l_pixel.x] / 255 + MODULATION_POS_OFFSET;
      l_radius += l_modulation;

      l_pixel.x = (uint16_t)(l_radius * sin(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_width / 2 + 1);
      l_pixel.y = (uint16_t)(l_radius * cos(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_height / 2 + 1);

      if(0 == l_rotation &&
         0 == l_angle)
      {
        if(is_position_empty(*first_spiral_start))
        {
          *first_spiral_start = l_pixel;
        }

        l_old_pixel = *first_spiral_start;
      }

      draw_line(l_pixel, l_old_pixel, picture_width, picture_map);
      l_old_pixel = l_pixel;
    }
  }

  l_last_pixel_in_first_rotation = l_pixel;
  angle += ANGLE_DIFF;
  init_position(&l_old_pixel);

  for(l_rotation = 0; FULL_CIRCLE * SPIRAL_DENSITY * l_rotation + FULL_CIRCLE * SPIRAL_DENSITY < MIN(picture_width / 2 - 10, picture_height / 2 - 10); l_rotation++)
  {
    l_is_last_rotation = FULL_CIRCLE * SPIRAL_DENSITY * (l_rotation + 1) + FULL_CIRCLE * SPIRAL_DENSITY >= MIN(picture_width / 2 - 10, picture_height / 2 - 10);
    l_angle_step = FULL_CIRCLE * atan(1 / (FULL_CIRCLE * SPIRAL_DENSITY * (l_rotation + 1))) / (2 * M_PI);

    for(l_angle = 0; l_angle < FULL_CIRCLE; l_angle += l_angle_step)
    {
      l_radius = FULL_CIRCLE * SPIRAL_DENSITY * l_rotation + l_angle * SPIRAL_DENSITY;

      l_pixel.x = (uint16_t)(l_radius * sin(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_width / 2 + 1);
      l_pixel.y = (uint16_t)(l_radius * cos(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_height / 2 + 1);

      l_modulation = MODULATION_FORCE * modulation_map[l_pixel.y * picture_width + l_pixel.x] / 255 - MODULATION_NEG_OFFSET;
      l_radius = MAX(0, l_radius - l_modulation);

      l_pixel.x = (uint16_t)(l_radius * sin(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_width / 2 + 1);
      l_pixel.y = (uint16_t)(l_radius * cos(2 * M_PI * (l_angle + angle) / FULL_CIRCLE) + picture_height / 2 + 1);

      if(0 == l_rotation &&
         0 == l_angle)
      {
        if(is_position_empty(*second_spiral_start))
        {
          *second_spiral_start = l_pixel;
        }

        l_old_pixel = *second_spiral_start;
      }

      if(!l_is_last_rotation ||
         (l_is_last_rotation && (l_angle + angle) < (angle + FULL_CIRCLE - ANGLE_DIFF)))
      {
        draw_line(l_pixel, l_old_pixel, picture_width, picture_map);
        l_old_pixel = l_pixel;
      }
    }
  }

  draw_line(l_old_pixel, l_last_pixel_in_first_rotation, picture_width, picture_map);
}

int read_input_picture(
  const char     *filename,
  const uint16_t  picture_width,
  const uint16_t  picture_height,
        uint8_t  *modulation_map
)
{
  const ujPlane* l_plane = NULL;
  ujImage l_img = ujCreate();

  memset(modulation_map, 0, sizeof(uint8_t) * picture_width * picture_height);

  if(NULL == ujDecodeFile(l_img, filename))
  {
    fprintf(stderr, "Unable to open %s\n", filename);

    return ERROR;
  }

  if(NULL == (l_plane = ujGetPlane(l_img, LUMINANCE)) ||
     NULL == l_plane->pixels)
  {
    fprintf(stderr, "Unable to decode %s\n", filename);

    return ERROR;
  }

  if(PIC_WIDTH < l_plane->width ||
     0 == l_plane->width)
  {
    fprintf(stderr, "Invalid input file width %d\n", l_plane->width);

    return ERROR;
  }

  if(PIC_HEIGHT < l_plane->height ||
     0 == l_plane->height)
  {
    fprintf(stderr, "Invalid input file height %d\n", l_plane->height);

    return ERROR;
  }

  const uint16_t l_height_compensation = (picture_height - l_plane->height) / 2;
  const uint16_t l_width_compensation = (picture_width - l_plane->width) / 2;

  for(int32_t i = 0; i < l_plane->height; ++i)
  {
    for(int32_t j = 0; j < l_plane->width; ++j)
    {
      modulation_map[(l_height_compensation + i) * picture_width + j + l_width_compensation] = 255 - l_plane->pixels[(i * l_plane->stride + j)];
    }
  }

  ujFree(l_img);

  return SUCCESS;
}

int generate_output_picture(
  const uint16_t  picture_width,
  const uint16_t  picture_height,
  const char     *out_filename,
  const uint8_t  *picture_map
)
{
  const uint32_t l_error = lodepng_encode_file(out_filename, picture_map, picture_width, picture_height, LCT_GREY, 8);

  if(l_error)
  {
    fprintf(stderr, "Error %u: %s\n", l_error, lodepng_error_text(l_error));

    return ERROR;
  }

  return SUCCESS;
}

void create_out_filename(
  const char *in_filename,
        char *out_filename
)
{
  uint8_t l_dot_position = 0;
  const char *l_suffix = "_spiro.png";

  for(; l_dot_position < MIN(strlen(in_filename), FILENAME_MAX - strlen(l_suffix) - 1); l_dot_position++)
  {
    if('.' == in_filename[l_dot_position])
    {
      break;
    }
  }

  memcpy(out_filename, in_filename, l_dot_position);
  memcpy(out_filename + l_dot_position, l_suffix, strlen(l_suffix));
}

int main(
  int   argc,
  char *argv[]
)
{
  int l_status = SUCCESS;
  char l_out_filename[FILENAME_MAX] = "";
  uint8_t *l_picture_map = NULL;
  uint8_t *l_modulation_map = NULL;
  position_t l_first_spiral_start;
  position_t l_second_spiral_start;

  if(2 != argc)
  {
    printf("Please specify input file!\nUsage: spiroglypher input_file_name.jpg\n");
    l_status = ERROR;
  }

  if(SUCCESS == l_status)
  {
    l_picture_map = malloc(sizeof(uint8_t) * PIC_WIDTH * PIC_HEIGHT);
    l_modulation_map = malloc(sizeof(uint8_t) * PIC_WIDTH * PIC_HEIGHT);

    if(NULL == l_picture_map ||
       NULL == l_modulation_map)
    {
      fprintf(stderr, "malloc() failed on line %d\n", __LINE__);
      l_status = ERROR;
    }
  }

  if(SUCCESS == l_status &&
     SUCCESS == (l_status = read_input_picture(argv[1], PIC_WIDTH, PIC_HEIGHT, l_modulation_map)))
  {
    memset(l_picture_map, 255, sizeof(uint8_t) * PIC_WIDTH * PIC_HEIGHT);

    init_position(&l_first_spiral_start);
    init_position(&l_second_spiral_start);

    create_spiral_pair(PIC_WIDTH, PIC_HEIGHT, l_modulation_map, 0, l_picture_map, &l_first_spiral_start, &l_second_spiral_start);
    create_spiral_pair(PIC_WIDTH, PIC_HEIGHT, l_modulation_map, 180, l_picture_map, &l_second_spiral_start, &l_first_spiral_start);

    create_out_filename(argv[1], l_out_filename);
    l_status = generate_output_picture(PIC_WIDTH, PIC_HEIGHT, l_out_filename, l_picture_map);
  }

  free(l_picture_map);
  free(l_modulation_map);

  return l_status;
}
