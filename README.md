This simple C program creates spiroglyphs /or whatever these pictures are called/. Simply make the executable by issuing ```make spiroglypher``` command and then run it by ```./spiroglypher INPUT_FILE```, the output is then ```INPUT_FILE_spiro.png```.

After coloring with ~~Photoshop~~ MS Paint:

![sample](sample.png)

Limitations:
  - The input file dimensions have to be smaller or equal than output file /5001 x 5001 pixels predefined/, but sure you can change these.

Feel free to play with the predefined constants, but these worked the best for me. As always, improvements are welcome.

The code uses JPEG decoder from Martin J. Fiedler and PNG encoder from Lode Vandevenne.
