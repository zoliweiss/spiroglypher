LDFLAGS = -lm
CFLAGS = -Wall -Wextra -O3 -std=c99

default: spiroglypher

spiroglypher: lodepng.o ujpeg.o spiroglypher.o
	$(CC) $(LDFLAGS) -o $@ $^

spiroglypher.o: spiroglypher.c
	$(CC) $(CFLAGS) -c -o $@ $<

lodepng.o: lodepng/lodepng.c lodepng/lodepng.h
	$(CC) $(CFLAGS) -DLODEPNG_NO_COMPILE_DECODER -c -o $@ $<

ujpeg.o: ujpeg/ujpeg.c ujpeg/ujpeg.h
	$(CC) $(CFLAGS) -Wno-shift-negative-value -c -o $@ $<

.PHONY: clean

clean:
	rm -f *.o spiroglypher
